package com.obhaiyya.health.presentation.utils

import android.content.Context
import com.obhaiyya.health.presentation.login.LoginActivity
import com.obhaiyya.health.presentation.splash.SplashActivity
import com.obhaiyya.health.presentation.splash.SubscriptionActivity
import com.obhaiyya.health.presentation.walkthrough.WalkthroughActivity

object Navigator {


    fun navigateToLogin(context: Context) {
        context.startActivity(LoginActivity.getCallingIntent(context))
    }

    fun navigateToWalkThrough(context: Context) {
        context.startActivity(WalkthroughActivity.getCallingIntent(context))
    }

    fun navigateToSplash(context: Context) {
        context.startActivity(SplashActivity.getCallingIntent(context))
    }

    fun navigateToSubscription(context: Context) {
        context.startActivity(SubscriptionActivity.getCallingIntent(context))
    }


}