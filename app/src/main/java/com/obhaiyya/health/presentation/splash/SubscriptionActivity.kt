package com.obhaiyya.health.presentation.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.obhaiyyahealth.R
import com.obhaiyya.health.presentation.base.BaseActivity

class SubscriptionActivity : BaseActivity(), View.OnClickListener {


    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, SubscriptionActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription)

    }

    override fun onClick(view: View?) {
        when (view?.id) {

        }
    }


}