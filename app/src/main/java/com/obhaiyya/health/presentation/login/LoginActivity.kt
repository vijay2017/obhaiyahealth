package com.obhaiyya.health.presentation.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.example.obhaiyyahealth.R
import com.obhaiyya.health.presentation.base.BaseActivity
import com.example.obhaiyyahealth.presentation.base.LoginVM
import com.obhaiyya.health.presentation.utils.Navigator
import kotlinx.android.synthetic.main.activity_login.*


/**
 * Created by Vijay on 7/3/20.
 */

class LoginActivity : BaseActivity(), View.OnClickListener {

    val mLoginVM: LoginVM by viewModels()
    var mobileNum = ""
    var mTermsAndConditionSelected = false


    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        txtGoogle.setOnClickListener(this)

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtGoogle -> {

                Navigator.navigateToWalkThrough(this)

            }
        }
    }


}