package com.obhaiyya.health.presentation.walkthrough

import com.appwithmeflutter.mype.presentation.base.BaseFragment
import com.example.obhaiyyahealth.R

class SecondWThroughFragment : BaseFragment() {
    override fun getLayout(): Int {
        return R.layout.fragment_secondwalkthrough
    }
}