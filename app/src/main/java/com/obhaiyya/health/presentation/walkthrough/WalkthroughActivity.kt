package com.obhaiyya.health.presentation.walkthrough

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.obhaiyyahealth.R
import com.example.obhaiyyahealth.presentation.base.LoginVM
import com.obhaiyya.health.presentation.utils.Navigator
import kotlinx.android.synthetic.main.activity_walkthrough_new.*


/**
 * Created by Vijay on 7/3/20.
 */

class WalkthroughActivity : FragmentActivity(), View.OnClickListener {

    val mLoginVM: LoginVM by viewModels()
    var mobileNum = ""
    var mTermsAndConditionSelected = false


    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, WalkthroughActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walkthrough_new)

        btn_next.setOnClickListener(this)
        btn_skip.setOnClickListener(this)

        val adapter = WalkThroughAdapter(this)
        view_pager.adapter = adapter

        view_pager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)

            }

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0, 1 -> {
                        btn_next.visibility = View.GONE
                        btn_skip.setVisibility(View.VISIBLE);
                    }
                    2 -> {
                        btn_next.visibility = View.VISIBLE
                        btn_skip.setVisibility(View.GONE);
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)

            }
        })

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_next -> {
                Navigator.navigateToSubscription(this)
            }
            R.id.btn_skip -> {
                Navigator.navigateToSubscription(this)
            }

        }
    }


}