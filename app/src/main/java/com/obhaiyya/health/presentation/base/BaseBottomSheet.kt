package com.obhaiyya.health.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.appwithmeflutter.mype.data.network.CustomerService
import com.example.obhaiyyahealth.R
import com.obhaiyya.health.data.SharedPreferenceManager
import com.example.obhaiyyahealth.dataProvider.ApiService
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Created by Vijay on 8/2/20.
 */

abstract class BaseBottomSheet : BottomSheetDialogFragment() {

    var mCustomerService: CustomerService? = null
    var mSharedPreferenceManager: SharedPreferenceManager? = null

    init {
        mSharedPreferenceManager =
            SharedPreferenceManager()
        mCustomerService = ApiService.getInstance()?.call()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = LayoutInflater.from(activity).inflate(getLayout(), container, false)
        return view
    }



    abstract fun getLayout(): Int

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
    }

    fun showToastMessage(msg: String?) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

}