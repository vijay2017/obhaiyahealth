package com.appwithmeflutter.mype.presentation.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.obhaiyyahealth.R
import com.obhaiyya.health.data.SharedPreferenceManager


/**
 * Created by Vijay on 4/4/20.
 */

abstract class BaseFragment : Fragment() {

    val mSharedManager =
        SharedPreferenceManager()
    var dialog: AlertDialog? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = LayoutInflater.from(activity).inflate(getLayout(), container, false)
        return view
    }

    abstract fun getLayout(): Int


    fun showProgress(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setView(R.layout.dialog_progress)
        dialog = builder.create()
        dialog!!.show()
    }

    fun hideProgress() {
        if (dialog?.isShowing == true) {
            dialog?.dismiss()
        }
    }

}