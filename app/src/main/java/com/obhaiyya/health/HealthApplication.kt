package com.obhaiyya.health

import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho

/**
 * Created by Vijay on 10/3/20.
 */

class HealthApplication : MultiDexApplication() {


    companion object {

        var mAppContext: HealthApplication? = null

        fun getAppInstance(): HealthApplication? {
            return mAppContext
        }

    }

    override fun onCreate() {
        super.onCreate()
        mAppContext = this
        Stetho.initializeWithDefaults(this)


    }


}